package com.education.ztu;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
       //System
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter first num: ");
        Double num1 = scanner.nextDouble();

        System.out.print("Enter second num: ");
        Double num2 = scanner.nextDouble();

        double res = num1 + num2;
        System.out.print("result: " + res);
        scanner.close();
    }
}
