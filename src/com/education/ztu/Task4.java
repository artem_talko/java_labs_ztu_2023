package com.education.ztu;
import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("enter first num: ");
        int num1 = scanner.nextInt();

        System.out.print("enter second num: ");
        int num2 = scanner.nextInt();

        System.out.print("result: " + findNSD(num1, num2));

        scanner.close();
    }

    public static int findNSD(int num1, int num2) {
        while (num2 != 0){
            int tempNum = num2;
            num2 = num1 % num2;
            num1 = tempNum;
        }
        return num1;
    }
}
