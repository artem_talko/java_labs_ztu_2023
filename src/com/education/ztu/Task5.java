package com.education.ztu;
import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter num: ");
        int number = scanner.nextInt();

        int digitSum = calculateDigitSum(number);

        System.out.println("Sum of digits in: " + number + " equal " + digitSum);

        scanner.close();
    }

    public static int calculateDigitSum(int n) {
        int sum = 0;

        while (n > 0) {
            int digit = n % 10;
            sum += digit;
            n /= 10;
        }

        return sum;
    }
}
