package com.education.ztu;
import java.util.Scanner;

public class Task6 {
    static int[] fibonacci(int number) {
        int[] result = new int[number];

        if (number >= 1) {
            result[0] = 1;
        }

        if (number >= 2) {
            result[1] = 1;
        }

        for (int i = 2; i < number; i++) {
            result[i] = result[i - 1] + result[i - 2];
        }

        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of Fibonacci numbers: ");
        int n = scanner.nextInt();

        int[] fibonacciArray = new int[n];
        fibonacciArray[0] = 1;
        if (n > 1) {
            fibonacciArray[1] = 1;
        }

        for (int i = 2; i < n; i++) {
            fibonacciArray[i] = fibonacciArray[i - 1] + fibonacciArray[i - 2];
        }

        int[] reverseFibonacciArray = new int[n];
        for (int i = 0; i < n; i++) {
            reverseFibonacciArray[i] = fibonacciArray[n - i - 1];
        }

        System.out.println("Array with increasing Fibonacci sequence:");
        printArray(fibonacciArray);

        System.out.println("Array with reverse Fibonacci sequence:");
        printArray(reverseFibonacciArray);

        scanner.close();
    }

    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]);
            if (i < array.length - 1) {
                System.out.print(", ");
            }
        }
        System.out.println();
    }
}

